﻿namespace CoffeeMachine
{
    public interface IRecipe
    {
        Cup Make();
    }
}
