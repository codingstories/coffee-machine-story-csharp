﻿namespace CoffeeMachine
{
    public abstract class CoffeeMakerBase: IRecipe
    {
        private ICupFactory cupFactory;

        public CoffeeMakerBase()
        {
            cupFactory = new CupFactory();
        }

        public CoffeeMakerBase(ICupFactory cupFactory)
        {
            this.cupFactory = cupFactory;
        }

        public abstract Cup Make();

        protected Cup TakeNewEmptyCup()
        {
            return cupFactory.GetEmptyCup();
        }
    }
}
