﻿namespace CoffeeMachine
{
    public class MocaccinoMaker: CoffeeMakerBase
    {
        public override Cup Make()
        {
            var cup = TakeNewEmptyCup();
            cup.Add("espresso");
            cup.Add("milk");
            cup.Add("milk");
            cup.Add("foam");
            cup.Add("chocolate");
            return cup;
        }
    }
}
