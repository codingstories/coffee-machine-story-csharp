﻿namespace CoffeeMachine
{
    public class EspressoMaker: CoffeeMakerBase
    {
        public override Cup Make()
        {
            var cup = TakeNewEmptyCup();
            cup.Add("espresso");
            return cup;
        }
    }
}
